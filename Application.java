import java.util.Scanner;

public class Application{
	public static void main(String[] args){
		Scanner reader = new Scanner(System.in);
		
		Album[] myCollection = new Album[4];
		
		for(int i=0;i<myCollection.length;i++){
			myCollection[i]= new Album();
			
			System.out.println("Enter album title.");
			myCollection[i].title = reader.nextLine();
			
			System.out.println("Enter album genre.");
			myCollection[i].genre = reader.nextLine();
			
			System.out.println("Enter album release year.");
			myCollection[i].releaseYear = Integer.parseInt(reader.nextLine());
		}
		System.out.println(myCollection[3].title);
		System.out.println(myCollection[3].genre);
		System.out.println(myCollection[3].releaseYear);
		
		myCollection[3].albumAge();
	}
}