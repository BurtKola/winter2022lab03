public class Album{
	public String title;
	public String genre;
	public int releaseYear;
	
	public void albumAge(){
		int age=2022-releaseYear;
		System.out.println(title+ " is " +age+ " years old!");
	}
}